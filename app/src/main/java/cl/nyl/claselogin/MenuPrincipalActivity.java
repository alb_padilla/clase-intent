package cl.nyl.claselogin;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuPrincipalActivity extends AppCompatActivity {
    private Button btn_llamar, btn_sms, btn_web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        btn_llamar = (Button)findViewById(R.id.btn_llamar);
        btn_sms = (Button) findViewById(R.id.btn_sms);
        btn_web = (Button) findViewById(R.id.btn_web);

        btn_llamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+956960747"));
                startActivity(intent);
            }
        });

        btn_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentt = new Intent(Intent.ACTION_VIEW);
                intentt.setData(Uri.parse("sms:"));
                intentt.setType("vnd.android-dir/mms-sms");
                intentt.putExtra("address",  "+56956969747");
                intentt.putExtra("sms_body", "JONA QL LAGI");
                startActivity(intentt);
            }
        });

        btn_web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://www.taringa.cl";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
    }
}
