package cl.nyl.claselogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private EditText txt_usuario, txt_pass;
    private Button btn_ingresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txt_usuario = (EditText)findViewById(R.id.txt_usuario);
        txt_pass = (EditText)findViewById(R.id.txt_pass);
        btn_ingresar = (Button)findViewById(R.id.btn_ingresar);

        btn_ingresar.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                validarUsuario();
            }

        });
    }

        private void validarUsuario(){
            if(txt_usuario.getText().toString().equals("Alberto") && txt_pass.getText().toString().equals("pass1"))
            {
                Toast.makeText(LoginActivity.this, "Bienvenido "+txt_usuario.getText().toString()+"!", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(this, MenuPrincipalActivity.class);
                startActivity(i);

            }
    else{
                String mensaje = getResources().getString(R.string.mensaje_error_login);
        Toast.makeText(LoginActivity.this, mensaje, Toast.LENGTH_LONG).show();

    }


    }
}

